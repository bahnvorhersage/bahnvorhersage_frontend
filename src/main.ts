import 'bootstrap/js/dist/collapse'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'

import { default as dayjs } from 'dayjs'
import { default as duration } from 'dayjs/plugin/duration'
import { default as utc } from 'dayjs/plugin/utc'
import { default as timezone } from 'dayjs/plugin/timezone'
dayjs.extend(duration)
dayjs.extend(utc)
dayjs.extend(timezone)

dayjs.tz.setDefault('Europe/Berlin')

import { registerSW } from 'virtual:pwa-register'

registerSW({
  onNeedRefresh() {},
  onOfflineReady() {}
})

const pinia = createPinia()

createApp(App).use(pinia).use(router).mount('#app')
