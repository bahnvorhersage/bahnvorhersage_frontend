import { defineStore } from 'pinia'
import flatpickr from 'flatpickr'
import router from '../router'
import { type Journey, type JourneyAndAlternative } from '../assets/ts/fptfTypes'
import { useLocalStorage } from '@vueuse/core'
import { Capacitor } from '@capacitor/core'

/**
 * https://stackoverflow.com/a/59806829/7246401
 */
export class SearchParams {
  start = ''
  destination = ''
  date = flatpickr.formatDate(new Date(), 'd.m.Y H:i')
  search_for_arrival = false
  only_regional = false
  bike = false
}

export class AlphaSearchParams {
  origin = ''
  destination = ''
  departure = new Date(new Date().setSeconds(0, 0)).toISOString() // when seconds are set, flatpickr mobile will fail on some validation
}

async function getJourneysWeb(searchParams: SearchParams) {
  return await fetch(__REQUEST_BASE_URL__ + 'api/journeys', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(searchParams)
  })
}

async function getJourneysMobile(searchParams: SearchParams) {
  const { getRatedJourneys } = await import('@/assets/ts/appDataGathering')

  return getRatedJourneys(searchParams)
}

async function refreshJourneysWeb(refreshToken: string) {
  const response = await fetch(__REQUEST_BASE_URL__ + 'api/refresh-journey', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ refresh_token: refreshToken })
  })

  return response
}

async function refreshJourneysMobile(journey: Journey) {
  const { refreshRatedJourney } = await import('@/assets/ts/appDataGathering')

  return refreshRatedJourney(journey)
}

export const useMainStore = defineStore('main', {
  state: () => ({
    stations: [] as string[],
    journeys: [] as Journey[],
    savedJourneys: useLocalStorage('saved-journeys', {} as Record<string, Journey>),
    progressing: false,
    error: null as Error | null,
    searchParams: new SearchParams(),
    alphaSearchParams: new AlphaSearchParams(),
    journeysAndAlternatives: [] as JourneyAndAlternative[]
  }),
  actions: {
    setError(error: Error) {
      this.error = error
      console.log(error)
      setTimeout(() => {
        this.error = null
      }, 15000)
    },
    saveJourney(journey: Journey) {
      if (journey.refreshToken in this.savedJourneys) {
        delete this.savedJourneys[journey.refreshToken]
      } else {
        this.savedJourneys[journey.refreshToken] = journey
      }
    },
    async displayFetchError(response: Response) {
      console.log(response)
      if (!response.ok) {
        let error
        if (response.status === 429) {
          error = Error(
            'Du hast zu viele Anfragen an unseren Server gesendet. Bitte warte ein paar Minuten und versuche es erneut.'
          )
        } else {
          try {
            error = Error((await response.json()).error || response.statusText)
          } catch (e) {
            error = Error(response.statusText)
          }
        }
        this.setError(error)
        this.progressing = false
        throw error
      }
      return response
    },
    async fetchStations() {
      if (!(this.stations.length > 0)) {
        let response = await fetch(__REQUEST_BASE_URL__ + 'api/station_list.json')
        response = await this.displayFetchError(response)
        this.stations = (await response.json()).stations
      }
    },
    async getJourneysAndAlternatives() {
      const response = await fetch(__REQUEST_BASE_URL__ + 'api/journeys', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.alphaSearchParams)
      })
      this.journeysAndAlternatives = await (await this.displayFetchError(response)).json()
    },
    async refreshSavedJourney(refreshToken: string) {
      this.progressing = true
      let journey

      if (Capacitor.isNativePlatform()) {
        const response = await refreshJourneysMobile(this.savedJourneys[refreshToken])
        journey = await (await this.displayFetchError(response)).json()

        if (journey.length !== 1) {
          throw Error('Expected exactly one journey, got ' + journey.length)
        }
        journey = journey[0]
      } else {
        const response = await refreshJourneysWeb(refreshToken)
        journey = await (await this.displayFetchError(response)).json()
      }

      if (refreshToken !== journey.refreshToken) {
        this.savedJourneys[journey.refreshToken] = journey
        delete this.savedJourneys[refreshToken]
      } else {
        this.savedJourneys[refreshToken] = journey
      }

      this.progressing = false
    },
    async getJourneys() {
      this.journeys = []
      this.progressing = true

      const response = Capacitor.isNativePlatform()
        ? await getJourneysMobile(this.searchParams)
        : await getJourneysWeb(this.searchParams)

      const journeys = await (await this.displayFetchError(response)).json()
      if (journeys) {
        this.journeys = journeys
        router.replace({ path: '/journey-search-result', hash: '#content' })
      }

      this.progressing = false
    }
  }
})
