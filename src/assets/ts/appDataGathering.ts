import { createClient } from 'db-vendo-client'
import { profile as dbnavProfile } from 'db-vendo-client/p/dbnav/index.js'
import { SearchParams } from '@/stores/main'
import type {
  HafasClient,
  JourneysOptionsCommon,
  Journey,
  Trip
} from '@/assets/ts/vendoClientTypes'
import { type Journey as JourneyBahnvorhersage } from '@/assets/ts/fptfTypes'

import dayjs from 'dayjs'

const userAgent = 'bahnvorhersage-app'
const client: HafasClient = createClient(dbnavProfile, userAgent, { enrichStations: false })

const commonJourneyOptions: JourneysOptionsCommon = {
  stopovers: true,
  tickets: true
}

async function stationNameToId(stationName: string) {
  const locations = await client.locations(stationName, {
    poi: false,
    addresses: false,
    fuzzy: false
  })

  for (const location of locations) {
    if (location.name === stationName) {
      return location.id
    }
  }
}

function parseJourneyOptions(searchParams: SearchParams) {
  let params: JourneysOptionsCommon = {
    bike: searchParams.bike,
    startWithWalking: true,
    ...commonJourneyOptions
  }

  if (searchParams.search_for_arrival) {
    params = {
      ...params,
      arrival: dayjs(searchParams.date, 'DD.MM.YYYY HH:mm').toDate()
    }
  } else {
    params = {
      ...params,
      departure: dayjs(searchParams.date, 'DD.MM.YYYY HH:mm').toDate()
    }
  }

  if (searchParams.only_regional) {
    params = {
      ...params,
      products: {
        national: false,
        nationalExpress: false
      }
    }
  }

  return searchParams
}

async function getJourneys(searchParams: SearchParams) {
  const [originId, destinationId] = await Promise.all([
    stationNameToId(searchParams.start),
    stationNameToId(searchParams.destination)
  ])

  if (originId === undefined) {
    throw new Error(`Could not find station ${searchParams.start}`)
  }
  if (destinationId === undefined) {
    throw new Error(`Could not find station ${searchParams.destination}`)
  }

  return client.journeys(originId, destinationId, parseJourneyOptions(searchParams))
}

async function refreshJourney(refreshToken: string | undefined) {
  if (refreshToken === undefined) {
    throw new Error('No refresh token provided')
  }
  return client.refreshJourney(refreshToken, { ...commonJourneyOptions })
}

async function getTrips(tripIds: string[]) {
  const trips = tripIds.map((tripId) => client.trip!(tripId, {}))
  const tripsById: { [index: string]: Trip } = {}
  for (const trip of await Promise.all(trips)) {
    tripsById[trip.trip.id] = trip.trip
  }
  return tripsById
}

function getTripIds(journeys: readonly Journey[]) {
  return journeys
    .map((journey) =>
      journey.legs
        .filter((leg) => leg.walking !== true && leg.type !== 'transfer')
        .map((leg) => {
          const tripId = leg.tripId
          if (tripId === undefined) {
            throw new Error('TripId missing for leg')
          } else {
            return tripId
          }
        })
    )
    .flat()
}

async function getJourneysAndTrips(searchParams: SearchParams) {
  const journeys = await getJourneys(searchParams)
  if (journeys.journeys === undefined) {
    throw new Error('No journeys found')
  }

  const tripIds = getTripIds(journeys.journeys)
  const trips = await getTrips(tripIds)

  return { journeys: journeys.journeys, trips: trips }
}

export async function getRatedJourneys(searchParams: SearchParams) {
  const params = await getJourneysAndTrips(searchParams)
  return fetch(__MOBILE_BASE_URL__ + 'journeys', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(params)
  })
}

export async function refreshRatedJourney(journeyToRefresh: JourneyBahnvorhersage) {
  const [journey, trips] = await Promise.all([
    refreshJourney(journeyToRefresh.refreshToken),
    getTrips(getTripIds([journeyToRefresh]))
  ])
  return fetch(__MOBILE_BASE_URL__ + 'journeys', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ journeys: [journey.journey], trips: trips })
  })
}
