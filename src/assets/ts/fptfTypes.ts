import type { Duration } from 'dayjs/plugin/duration'

export type Stop = {
  type: 'stop'
  id: string
  name: string
}

export type Stopover = {
  type: 'stopover'
  stop: Stop
  arrival?: string
  plannedArrival?: string
  arrivalPlatform: string
  departure?: string
  plannedDeparture?: string
  departurePlatform: string
  cancelled?: boolean
}

export type Line = {
  type: 'line'
  id: string
  name: string
  operator: string
  isRegio: boolean
  productName: string
  mode: string
  fahrtNr?: string
  adminCode?: string
}

export type DelayPrediction = {
  type: 'delayPrediction'
  predictions: number[]
  offset: number
}

export type Leg = {
  type: 'leg'
  origin: Stop
  destination: Stop
  departure?: string
  departureDelayPrediction?: DelayPrediction
  plannedDeparture?: string
  departurePlatform?: string
  plannedDeparturePlatform?: string
  arrival?: string
  arrivalDelayPrediction?: DelayPrediction
  plannedArrival?: string
  arrivalPlatform?: string
  plannedArrivalPlatform?: string
  stopovers: Stopover[]
  mode: string
  public: boolean
  line: Line
  direction?: string
  tripId: string
}

export type Transfer = {
  type: 'transfer'
  origin: Stop
  destination: Stop
  duration: string
  distanceMeters?: number
  source: 'RIL420' | 'INDOOR_ROUTING' | 'EFZ' | 'FALLBACK' | 'HAFAS'
  identicalPhysicalPlatform?: boolean
  mode?: 'walk' | 'bike'
  transferScore?: number
}

export type Price = {
  type: 'price'
  amount: number
  currency: string
}

export type Journey = {
  type: 'journey'
  legs: (Leg | Transfer)[]
  price?: Price
  refreshToken: string
  saved?: boolean
}

export type JourneyAndAlternative = {
  journey: Journey
  alternatives: Journey[]
}

export type JourneySegmentSummary = {
  label: string
  duration: Duration
  plannedDuration?: Duration
}
