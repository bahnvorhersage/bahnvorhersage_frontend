import dayjs, { duration, Dayjs } from 'dayjs'
import type { Duration } from 'dayjs/plugin/duration'
import {
  type Leg,
  type Journey,
  type DelayPrediction,
  type JourneySegmentSummary
} from './fptfTypes'

export function getDurationsOfLeg(leg: Leg) {
  return {
    duration: dayjs.duration(dayjs(leg.arrival).diff(leg.departure)),
    ...(leg.plannedArrival &&
      leg.plannedDeparture && {
        plannedDuration: dayjs.duration(dayjs(leg.plannedArrival).diff(leg.plannedDeparture))
      })
  }
}

export function getExpectedDelay(
  time: Dayjs,
  plannedTime?: Dayjs,
  delayPrediction?: DelayPrediction
) {
  const currentDelay = dayjs.duration(time.diff(plannedTime ?? time)).asMinutes()
  if (!delayPrediction) {
    return currentDelay
  } else {
    let averageDelayDiff = 0
    for (let i = 0; i < delayPrediction.predictions.length; i++) {
      averageDelayDiff += delayPrediction.predictions[i] * (i - delayPrediction.offset)
    }
    return currentDelay + averageDelayDiff
  }
}

export function getJourneyDeparture(journey: Journey) {
  let departure, plannedDeparture
  const startTransferDuration = duration(0)
  for (const leg of journey.legs) {
    if (leg.type === 'transfer') {
      startTransferDuration.add(duration(leg.duration))
    } else if (leg.type === 'leg') {
      departure = leg.departure
      plannedDeparture = leg.plannedDeparture
      break
    }
  }

  return {
    time: dayjs(departure!).tz('Europe/Berlin').subtract(startTransferDuration),
    ...(plannedDeparture && {
      plannedTime: dayjs(plannedDeparture).tz('Europe/Berlin').subtract(startTransferDuration)
    })
  }
}

export function getJourneyArrival(journey: Journey) {
  let arrival, plannedArrival
  const endTransferDuration = duration(0)
  for (const leg of journey.legs.slice().reverse()) {
    if (leg.type === 'transfer') {
      endTransferDuration.add(duration(leg.duration))
    } else if (leg.type === 'leg') {
      arrival = leg.arrival
      plannedArrival = leg.plannedArrival
      break
    }
  }

  return {
    time: dayjs(arrival!).tz('Europe/Berlin').add(endTransferDuration),
    ...(plannedArrival && {
      plannedTime: dayjs(plannedArrival).tz('Europe/Berlin').add(endTransferDuration)
    })
  }
}

export function getDurationsOfJourney(journey: Journey): {
  duration: Duration
  plannedDuration?: Duration
} {
  const departure = getJourneyDeparture(journey)
  const arrival = getJourneyArrival(journey)

  return {
    duration: dayjs.duration(dayjs(arrival.time).diff(departure.time)),
    ...(arrival.plannedTime &&
      departure.plannedTime && {
        plannedDuration: dayjs.duration(dayjs(arrival.plannedTime).diff(departure.plannedTime))
      })
  }
}

export function getJourneySummary(journey: Journey) {
  const durations = getDurationsOfJourney(journey)
  const categories = []
  const journeySegmentSummaries: JourneySegmentSummary[] = []
  let transfers = -1
  let connectionScore = 1

  for (const leg of journey.legs) {
    if (leg.type === 'leg') {
      categories.push(leg.line.productName)
      journeySegmentSummaries.push({
        label: leg.line.productName,
        ...getDurationsOfLeg(leg)
      })
      transfers++
    } else if (leg.type === 'transfer') {
      if (leg.transferScore) {
        connectionScore *= leg.transferScore
      }
    }
  }
  connectionScore = Math.trunc(connectionScore * 100)

  return {
    categories,
    transfers,
    connectionScore,
    journeySegmentSummaries,
    ...durations
  }
}
