import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'

let requestBaseUrl = ''
let mobileBaseUrl = ''
if (process.env.NODE_ENV === 'development') {
  requestBaseUrl = '/'
  mobileBaseUrl = 'http://192.168.2.126:5000/api/mobile/v1/'
} else if (process.env.DEV_SERVER === '1') {
  requestBaseUrl = 'https://next.bahnvorhersage.de/'
  mobileBaseUrl = 'https://next.bahnvorhersage.de/api/mobile/v1/'
} else {
  requestBaseUrl = 'https://bahnvorhersage.de/'
  mobileBaseUrl = 'https://bahnvorhersage.de/api/mobile/v1/'
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      injectRegister: 'auto',
      includeAssets: ['favicon.ico', 'apple-touch-icon.png', 'mask-icon.svg'],
      workbox: {
        navigateFallbackDenylist: [/^\/api/]
      },
      manifest: {
        name: 'Bahn-Vorhersage',
        short_name: 'Bahn-Vorhersage',
        description:
          'Rankingsystem für Zugverbindungen nach ihrer Anschlusssicherheit basierend auf Machine Learning',
        theme_color: '#3f51b5',
        background_color: '#212121',
        lang: 'de',
        icons: [
          {
            src: '/img/icons/android-chrome-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: '/img/icons/android-chrome-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
      }
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  define: {
    __REQUEST_BASE_URL__: JSON.stringify(requestBaseUrl),
    __MOBILE_BASE_URL__: JSON.stringify(mobileBaseUrl)
  },
  build: {
    target: 'es2023',
    sourcemap: true
  },
  server: {
    proxy: {
      // '/api': 'https://next.bahnvorhersage.de'
      //'/api': 'http://localhost:5000'
      '/api': 'https://bahnvorhersage.de'
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        silenceDeprecations: ['color-4-api', 'import', 'mixed-decls', 'color-functions'],
        quietDeps: true,
        additionalData: `
          @use 'sass:color';
          @import '@/assets/scss/variables.scss';
          @import 'bootstrap/scss/_functions.scss'; @import 'bootstrap/scss/_variables.scss'; @import 'bootstrap/scss/_mixins.scss';
      `
      }
    }
  }
})
