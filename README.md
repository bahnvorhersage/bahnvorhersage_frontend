# Bahn-Vorhersage Frontend https://bahnvorhersage.de

```
████████████████████████████████████▇▆▅▃▁
       Bahn-Vorhersage      ███████▙  ▜██▆▁
███████████████████████████████████████████▃
▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀█████▄▖
█████████████████████████████████████████████
 ▜█▀▀▜█▘                       ▜█▀▀▜█▘   ▀▀▀
```

This is the frontend for https://bahnvorhersage.de.

# Instructions

## Development setup

### Requirements

- nodejs and npm
- backend running at http://localhost:5000/, or an appropriate backend set in `vite.config.ts`

### Install

```bash
npm install
```

### Debugging

```bash
npm run serve
```

You may also need to lint the code:

```bash
npm run lint
```

### Generate Icon font
```bash
fantasticon src/assets/fonts/icons -o src/assets/fonts/icons  --asset-types scss --fonts-url @/assets/fonts/icons
```
**Important:** Check if `"bicycle": "\f104"` or `"person-walking-solid": "\f10e"` changed to a different code in `icons.scss`. If it did, change it in `JourneyAndAlternativeDisplay.vue`